﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SortingAlgorithms
{
    public class Sorter
    {
        public int[] InputArray { get; set; }
        public string Sequence { get; set; }
        public int MergeSortCalls { get; set; }
        public int MergeCalls { get; set; }

        public Sorter(int[] inputArray)
        {
            this.InputArray = inputArray;
            this.Sequence = "";
            this.MergeSortCalls = 0;
            this.MergeCalls = 0;
        }

        public string InsertionSort() //returns a string for easier visualization of sorting steps
        {
            int[] inputArray = InputArray;
            int steps = 0;
            for(int i = 1; i < inputArray.Length; i++)
            {
                int Key = inputArray[i];
                for(int j = i - 1 ; j >= 0; j--)
                {
                    if(Key < inputArray[j])
                    {
                        inputArray[j+1] = inputArray[j];
                        inputArray[j] = Key;
                        steps++;
                    }
                    

                }
                foreach (int element in inputArray) //this adds to algorithm complexity
                {
                    Sequence = Sequence + element.ToString() + ";";
                }
                Sequence = Sequence + "\r\n";
                

            }
            Sequence = Sequence + "\r\n" + "Concluded in " + steps + " shifting operations." ;
            return Sequence;
        }

        private void Merge(int[] input, int left, int middle, int right)
        {
            MergeCalls++;
            int[] leftArray = new int[middle - left + 1];
            int[] rightArray = new int[right - middle];

            Array.Copy(input, left, leftArray, 0, middle - left + 1);
            Array.Copy(input, middle + 1, rightArray, 0, right - middle);

            int i = 0;
            int j = 0;
            for (int k = left; k < right + 1; k++)
            {
                if (i == leftArray.Length)
                {
                    input[k] = rightArray[j];
                    j++;
                }
                else if (j == rightArray.Length)
                {
                    input[k] = leftArray[i];
                    i++;
                }
                else if (leftArray[i] <= rightArray[j])
                {
                    input[k] = leftArray[i];
                    i++;
                }
                else
                {
                    input[k] = rightArray[j];
                    j++;
                }
            }
        }

        public void MergeSort(int[] input, int left, int right) //this method doesnt use the class property InputArray, the array to be sorted must be passed externally
        {
            MergeSortCalls++;
            if (left < right)
            {
                int middle = (left + right) / 2;

                MergeSort(input, left, middle);
                MergeSort(input, middle + 1, right);

                Merge(input, left, middle, right);
            }
        }


    }

}
