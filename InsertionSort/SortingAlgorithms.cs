﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SortingAlgorithms
{
    public partial class SortingAlgorithms : Form
    {
        public SortingAlgorithms()
        {
            InitializeComponent();
            textBox_Output.ScrollBars = ScrollBars.Both;
            textBox_Output.WordWrap = false;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedText = "--Select--";
            numericUpDown_MaxValue.Value = 10;
            numericUpDown_Elements.Value = 10;
        }

        private void GroupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void Btn_Sort_Click(object sender, EventArgs e)
        {
            var watch = System.Diagnostics.Stopwatch.StartNew();
            // the code that you want to measure comes here

            string[] InputArray = TextBox_Input.Text.Split(';');
            int[] InputArrayInt = Array.ConvertAll(InputArray, int.Parse);
            Sorter mySorter = new Sorter(InputArrayInt);

            if (comboBox1.Text == "Insertion Sort")
            {

                string SortingSequence = mySorter.InsertionSort();
                textBox_Output.Text = SortingSequence;
            }else if(comboBox1.Text == "Merge Sort")
            {
                int end = InputArray.Length - 1;
                mySorter.MergeSort(InputArrayInt, 0, end);
                textBox_Output.Text = "Merge-sorted values:" + Environment.NewLine;
                for (int i = 0; i < InputArrayInt.Length; i++)
                    textBox_Output.Text += (InputArrayInt[i]) + ";";
                textBox_Output.Text += Environment.NewLine + $"MergeSort() was called {mySorter.MergeSortCalls} times and Merge() was called {mySorter.MergeCalls} times.";
            }
            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;
            textBox_Output.Text += Environment.NewLine + $"Completed in {elapsedMs}ms";
        }

        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Btn_Random_Click(object sender, EventArgs e)
        {
            TextBox_Input.Text = "";
            int count = Convert.ToInt32(numericUpDown_MaxValue.Value);
            Random rnd = new Random();
            for (int i =0; i< numericUpDown_Elements.Value; i++)
            {
                if (i == numericUpDown_Elements.Value - 1)
                {
                    TextBox_Input.Text = TextBox_Input.Text + rnd.Next(count);
                }
                else
                {
                    TextBox_Input.Text = TextBox_Input.Text + rnd.Next(count) + ";";
                }
            }
        }

        private void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDown_MaxValue_ValueChanged(object sender, EventArgs e)
        {

        }
    }
}
