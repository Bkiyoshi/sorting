﻿namespace SortingAlgorithms
{
    partial class SortingAlgorithms
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Sort = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.numericUpDown_MaxValue = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_Random = new System.Windows.Forms.Button();
            this.numericUpDown_Elements = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.TextBox_Input = new System.Windows.Forms.TextBox();
            this.textBox_Output = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MaxValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Elements)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Sort
            // 
            this.btn_Sort.ForeColor = System.Drawing.Color.Black;
            this.btn_Sort.Location = new System.Drawing.Point(154, 162);
            this.btn_Sort.Name = "btn_Sort";
            this.btn_Sort.Size = new System.Drawing.Size(99, 21);
            this.btn_Sort.TabIndex = 4;
            this.btn_Sort.Text = "Sort";
            this.btn_Sort.UseVisualStyleBackColor = true;
            this.btn_Sort.Click += new System.EventHandler(this.Btn_Sort_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(36, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(31, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Input";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.numericUpDown_MaxValue);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btn_Random);
            this.groupBox1.Controls.Add(this.numericUpDown_Elements);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.TextBox_Input);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(20, 23);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(233, 111);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Input";
            // 
            // numericUpDown_MaxValue
            // 
            this.numericUpDown_MaxValue.Location = new System.Drawing.Point(73, 79);
            this.numericUpDown_MaxValue.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_MaxValue.Name = "numericUpDown_MaxValue";
            this.numericUpDown_MaxValue.Size = new System.Drawing.Size(54, 20);
            this.numericUpDown_MaxValue.TabIndex = 6;
            this.numericUpDown_MaxValue.ValueChanged += new System.EventHandler(this.numericUpDown_MaxValue_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Max value";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // btn_Random
            // 
            this.btn_Random.Location = new System.Drawing.Point(134, 53);
            this.btn_Random.Name = "btn_Random";
            this.btn_Random.Size = new System.Drawing.Size(75, 46);
            this.btn_Random.TabIndex = 4;
            this.btn_Random.Text = "Random";
            this.btn_Random.UseVisualStyleBackColor = true;
            this.btn_Random.Click += new System.EventHandler(this.Btn_Random_Click);
            // 
            // numericUpDown_Elements
            // 
            this.numericUpDown_Elements.Location = new System.Drawing.Point(73, 53);
            this.numericUpDown_Elements.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Elements.Name = "numericUpDown_Elements";
            this.numericUpDown_Elements.Size = new System.Drawing.Size(54, 20);
            this.numericUpDown_Elements.TabIndex = 3;
            this.numericUpDown_Elements.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Elements";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // TextBox_Input
            // 
            this.TextBox_Input.Location = new System.Drawing.Point(73, 22);
            this.TextBox_Input.Name = "TextBox_Input";
            this.TextBox_Input.Size = new System.Drawing.Size(136, 20);
            this.TextBox_Input.TabIndex = 1;
            // 
            // textBox_Output
            // 
            this.textBox_Output.Location = new System.Drawing.Point(6, 19);
            this.textBox_Output.Multiline = true;
            this.textBox_Output.Name = "textBox_Output";
            this.textBox_Output.Size = new System.Drawing.Size(1351, 373);
            this.textBox_Output.TabIndex = 2;
            this.textBox_Output.TextChanged += new System.EventHandler(this.TextBox2_TextChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.textBox_Output);
            this.groupBox2.Location = new System.Drawing.Point(279, 23);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1378, 399);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Output";
            this.groupBox2.Enter += new System.EventHandler(this.GroupBox2_Enter);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Insertion Sort",
            "Merge Sort"});
            this.comboBox1.Location = new System.Drawing.Point(35, 162);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(112, 21);
            this.comboBox1.TabIndex = 5;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.ComboBox1_SelectedIndexChanged);
            // 
            // SortingAlgorithms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1669, 450);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.btn_Sort);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Name = "SortingAlgorithms";
            this.Text = "SortingAlgorithms";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_MaxValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Elements)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox TextBox_Input;
        private System.Windows.Forms.NumericUpDown numericUpDown_Elements;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Random;
        private System.Windows.Forms.TextBox textBox_Output;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.NumericUpDown numericUpDown_MaxValue;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_Sort;
    }
}

